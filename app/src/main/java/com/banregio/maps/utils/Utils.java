package com.banregio.maps.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

/**
 * Created by AcheDev on 5/14/18.
 */

public final class Utils {

    public static void requestPermission(Context context, PermissionListener permissionlistener, String deniedMessage , String... permissions){

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            TedPermission.with(context)
                    .setPermissionListener(permissionlistener)
                    .setPermissions(permissions)
                    .setDeniedMessage(deniedMessage)
                    .setGotoSettingButton(true)
                    .check();
        }
    }

    public static boolean checkSelfPermission(Context appContext, String... permissions){
        for (String p: permissions) {
            if(ContextCompat.checkSelfPermission(appContext,p) == PackageManager.PERMISSION_DENIED)
                return false;
        }
        return true;
    }
}
