package com.banregio.maps.beans;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by AcheDev on 5/12/18.
 */

public class User extends BaseObservable {
    private String user;
    private String password;
    @JsonIgnore
    private String userNameTextError;
    @JsonIgnore
    private String passwordTextError;

    @Bindable
    public String getUser() {
        return user;
    }

    @Bindable
    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Bindable
    @JsonIgnore
    public String getUserNameTextError() {
        return userNameTextError;
    }

    @Bindable
    @JsonIgnore
    public String getPasswordTextError() {
        return passwordTextError;
    }

    @JsonIgnore
    public boolean isValidUserName(){
        if (this.user != null && !this.user.isEmpty())
            this.userNameTextError = null;
        else
            this.userNameTextError = "User Name is required";

        notifyChange();
        return this.user != null && !this.user.isEmpty();
    }

    @JsonIgnore
    public boolean isValidPassword(){
        if (this.password != null && !this.password.isEmpty())
            this.passwordTextError = null;
        else
            this.passwordTextError = "Password is required";

        notifyChange();
        return this.password != null && !this.password.isEmpty();
    }

    @JsonIgnore
    public boolean isValidUser(){
        return this.isValidUserName() & this.isValidPassword();
    }
}
