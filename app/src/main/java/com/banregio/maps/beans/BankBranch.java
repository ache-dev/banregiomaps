package com.banregio.maps.beans;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.banregio.maps.R;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by AcheDev on 5/13/18.
 */
public class BankBranch extends BaseObservable implements Serializable{
    @JsonProperty("URL_FOTO")
    private String urlFoto;

    @JsonProperty("Latitud")
    private String latitud;

    @JsonProperty("NOMBRE")
    private String nombre;

    @JsonProperty("DOMICILIO")
    private String domicilio;

    @JsonProperty("24_HORAS")
    private String is24Hours;

    @JsonProperty("SABADOS")
    private String sabados;

    @JsonProperty("CIUDAD")
    private String ciudad;

    @JsonProperty("Longitud")
    private String longitud;

    @JsonProperty("TELEFONO_PORTAL")
    private String telefonoPortal;

    @JsonProperty("Correo_Gerente")
    private String correoGerente;

    @JsonProperty("HORARIO")
    private String horario;

    @JsonProperty("TELEFONO_APP")
    private String telefonoApp;

    @JsonProperty("ESTADO")
    private String estado;

    @JsonProperty("Suc_Estado_Prioridad")
    private String sucEstadoPrioridad;

    private String tipo;

    @JsonProperty("ID")
    private String id;

    @JsonProperty("Suc_Ciudad_Prioridad")
    private String sucCiudadPrioridad;

    @JsonIgnore
    private Double distance;

    @JsonIgnore
    private Context context;

    @JsonIgnore
    private Drawable logo;

    @JsonIgnore
    private boolean isATM;

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String Latitud) {
        this.latitud = Latitud;
    }

    @Bindable
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String NOMBRE) {
        this.nombre = NOMBRE;
    }

    @Bindable
    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String DOMICILIO) {
        this.domicilio = DOMICILIO;
    }

    public String getIs24Hours() {
        return is24Hours;
    }

    public void setIs24Hours(String _24_HORAS) {
        this.is24Hours = _24_HORAS;
    }

    public String getSabados() {
        return sabados;
    }

    public void setSabados(String SABADOS) {
        this.sabados = SABADOS;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String CIUDAD) {
        this.ciudad = CIUDAD;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String Longitud) {
        this.longitud = Longitud;
    }

    @Bindable
    public String getTelefonoPortal() {
        return telefonoPortal;
    }

    public void setTelefonoPortal(String TELEFONO_PORTAL) {
        this.telefonoPortal = TELEFONO_PORTAL;
    }

    @Bindable
    public String getCorreoGerente() {
        return correoGerente;
    }

    public void setCorreoGerente(String Correo_Gerente) {
        this.correoGerente = Correo_Gerente;
    }

    @Bindable
    public String getHorario() {
        return horario;
    }

    public void setHorario(String HORARIO) {
        this.horario = HORARIO;
    }

    @Bindable
    public String getTelefonoApp() {
        return telefonoApp;
    }

    public void setTelefonoApp(String TELEFONO_APP) {
        this.telefonoApp = TELEFONO_APP;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String ESTADO) {
        this.estado = ESTADO;
    }

    public String getSucEstadoPrioridad() {
        return sucEstadoPrioridad;
    }

    public void setSucEstadoPrioridad(String Suc_Estado_Prioridad) {
        this.sucEstadoPrioridad = Suc_Estado_Prioridad;
    }

    @Bindable
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getId() {
        return id;
    }

    public void setId(String ID) {
        this.id = ID;
    }

    public String getSucCiudadPrioridad() {
        return sucCiudadPrioridad;
    }

    public void setSucCiudadPrioridad(String sucCiudadPrioridad) {
        this.sucCiudadPrioridad = sucCiudadPrioridad;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @JsonIgnore
    public int getIdMarkerMipmap(){
        if (this.tipo.toLowerCase().equals("s"))
            return R.mipmap.ic_bank;
        else if (this.tipo.toLowerCase().equals("c"))
            return R.mipmap.ic_atm;
        else
            return R.mipmap.ic_bank;
    }

    @JsonIgnore
    @Bindable
    public Drawable getLogo(){
        if (this.tipo.toLowerCase().equals("s"))
            return ContextCompat.getDrawable(context, R.drawable.ic_bank);
        else
            return ContextCompat.getDrawable(context, R.drawable.ic_atm);
    }

    @Bindable
    public boolean isATM() {
        if (this.tipo.toLowerCase().equals("c"))
            return true;
        else
            return false;
    }
}
