package com.banregio.maps.ui.branches;

import android.location.Location;

import com.banregio.maps.beans.BankBranch;

import java.util.ArrayList;

/**
 * Created by AcheDev on 5/13/18.
 */

public interface BankBranchesService {

    void getBankBranches();

    interface BankBranchesServiceListener {
        void onSuccessGetBranches(ArrayList<BankBranch> bankBranches);
        void onFailureGetBranches();
        void onErrorGetBranches();
    }
}
