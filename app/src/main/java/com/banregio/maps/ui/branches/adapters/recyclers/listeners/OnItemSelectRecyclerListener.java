package com.banregio.maps.ui.branches.adapters.recyclers.listeners;

/**
 * Created by AcheDev on 5/13/18.
 */

public interface OnItemSelectRecyclerListener {
    void onItemSelect(int position);
}
