package com.banregio.maps.ui.login;

import com.banregio.maps.beans.User;

/**
 * Created by AcheDev on 5/12/18.
 */

public interface LoginPresenter {
    void signIn(User user);
}
