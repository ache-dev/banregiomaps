package com.banregio.maps.ui.login;

import android.content.Intent;
import android.databinding.Bindable;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;

import com.banregio.maps.R;
import com.banregio.maps.beans.User;
import com.banregio.maps.ui.branches.BranchesActivity;
import com.banregio.maps.databinding.ActivityLoginBinding;

public class LoginActivity extends AppCompatActivity implements LoginView{
    private final LoginPresenterImpl presenter = new LoginPresenterImpl(this);
    private final User mUser = new User();
    public final ObservableField<Boolean> uthenticating = new ObservableField(false);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.LoginTheme);
        ActivityLoginBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        binding.setLoginView(this);
        binding.setUser(mUser);
        binding.setLoginActivity(this);
    }

    @Override
    public View.OnClickListener signIn() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "SIGN IN", Toast.LENGTH_SHORT).show();
                presenter.signIn(mUser);
            }
        };
    }

    @Override
    public TextWatcher inputUser() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mUser.setUser(String.valueOf(charSequence));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
    }

    @Override
    public TextWatcher inputPassword() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mUser.setPassword(String.valueOf(charSequence));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void navigateToHome() {
        Intent intent = new Intent(getApplicationContext(), BranchesActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void setIsAuthenticating(boolean isAuthenticating){
        this.uthenticating.set(isAuthenticating);
    }

    @Override
    public boolean isAuthenticating() {
        return this.uthenticating.get();
    }
}

