package com.banregio.maps.ui.branches.adapters.recyclers.holders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.banregio.maps.R;
import com.banregio.maps.ui.branches.adapters.recyclers.listeners.OnItemSelectRecyclerListener;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by AcheDev on 5/13/18.
 */

public class BankBranchHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    @BindView(R.id.txv_bank_branch_name) TextView txvBankBranchName;
    @BindView(R.id.txv_bank_branch_address) TextView txvAddress;
    @BindView(R.id.txv_bank_branch_schedule) TextView txvSchedule;
    @BindView(R.id.txv_bank_branch_distance) TextView txvDistance;

    private OnItemSelectRecyclerListener onItemSelectRecyclerListener;
    private Context context;

    public BankBranchHolder(View itemView, OnItemSelectRecyclerListener onItemSelectRecyclerListener) {
        super(itemView);
        ButterKnife.bind(this,itemView);

        this.context = itemView.getContext();
        itemView.setOnClickListener(this);
        this.onItemSelectRecyclerListener = onItemSelectRecyclerListener;
    }

    public TextView getTxvBankBranchName() {
        return txvBankBranchName;
    }

    public void setTxvBankBranchName(TextView txvBankBranchName) {
        this.txvBankBranchName = txvBankBranchName;
    }

    public TextView getTxvAddress() {
        return txvAddress;
    }

    public void setTxvAddress(TextView txvAddress) {
        this.txvAddress = txvAddress;
    }

    public TextView getTxvSchedule() {
        return txvSchedule;
    }

    public void setTxvSchedule(TextView txvSchedule) {
        this.txvSchedule = txvSchedule;
    }

    public TextView getTxvDistance() {
        return txvDistance;
    }

    public void setTxvDistance(TextView txvDistance) {
        this.txvDistance = txvDistance;
    }

    public Context getContext(){
        return this.context;
    }

    @Override
    public void onClick(View view) {
        if (this.onItemSelectRecyclerListener != null)
            this.onItemSelectRecyclerListener.onItemSelect(getAdapterPosition());
    }
}
