package com.banregio.maps.ui.branches;

import android.app.Activity;
import android.content.Context;
import android.location.Location;

import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by AcheDev on 5/12/18.
 */

public interface BankBranchesPresenter {

    void loadBankBranches();

    void onCreate(Context context, Activity activity);

    void requestMyLocation();

    void requestAccessFineLocation();

    void setUpGClient();

}
