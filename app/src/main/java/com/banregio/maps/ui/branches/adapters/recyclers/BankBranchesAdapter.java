package com.banregio.maps.ui.branches.adapters.recyclers;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.banregio.maps.R;
import com.banregio.maps.beans.BankBranch;
import com.banregio.maps.ui.branches.BankBranchesView;
import com.banregio.maps.ui.branches.adapters.recyclers.holders.BankBranchHolder;
import com.banregio.maps.ui.branches.adapters.recyclers.listeners.OnItemSelectRecyclerListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AcheDev on 5/13/18.
 */

public class BankBranchesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements OnItemSelectRecyclerListener, Filterable {
    private final String LOG_CAT = this.getClass().getCanonicalName();

    ArrayList<BankBranch> bankBranches = new ArrayList<>();
    ArrayList<BankBranch> bankBranchesFiltered = new ArrayList<>();
    BankBranchesView bankBranchesView;

    public BankBranchesAdapter(ArrayList<BankBranch> bankBranches, BankBranchesView bankBranchesView) {
        this.bankBranches = bankBranches;
        this.bankBranchesFiltered = bankBranches;
        this.bankBranchesView = bankBranchesView;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.bank_branch_item, parent, false);
        return new BankBranchHolder(itemView, this);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        BankBranch bb = this.bankBranches.get(position);
        ((BankBranchHolder) holder).getTxvBankBranchName().setText(bb.getNombre());
        ((BankBranchHolder) holder).getTxvAddress().setText(bb.getDomicilio());
        ((BankBranchHolder) holder).getTxvSchedule().setText(bb.getHorario());
        ((BankBranchHolder) holder).getTxvDistance().setText(
                ((BankBranchHolder) holder).getContext().getResources().getString(R.string.distance_message, bb.getDistance())
        );
    }

    @Override
    public int getItemCount() {
        return this.bankBranches.size();
    }

    @Override
    public void onItemSelect(int position) {
        if (this.bankBranchesView != null)
            this.bankBranchesView.goToBankBranchDetail(this.bankBranches.get(position));
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    bankBranches = bankBranchesFiltered;
                } else {
                    ArrayList<BankBranch> filteredList = new ArrayList<>();
                    for (BankBranch row : bankBranchesFiltered) {
                        if (row.getNombre().toLowerCase().contains(charString.toLowerCase()) || row.getDomicilio().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    bankBranches = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = bankBranches;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                bankBranches = (ArrayList<BankBranch>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
