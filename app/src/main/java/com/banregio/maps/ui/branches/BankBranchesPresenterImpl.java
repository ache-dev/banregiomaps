package com.banregio.maps.ui.branches;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.banregio.maps.beans.BankBranch;
import com.banregio.maps.utils.GI;
import com.banregio.maps.utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.gun0912.tedpermission.PermissionListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by AcheDev on 5/12/18.
 */
public class BankBranchesPresenterImpl implements BankBranchesPresenter, BankBranchesService.BankBranchesServiceListener, PermissionListener, LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    private final String LOG_CAT = this.getClass().getCanonicalName();

    private BankBranchesView bankBranchesView;
    private final BankBranchesService branchesService = new BankBranchesServiceImpl(this);
    private GoogleApiClient googleApiClient;
    private Location myLocation;
    private LocationManager locationManager;
    private Context context;
    private Activity activity;

    public final static int REQUEST_CHECK_SETTINGS_GPS=0x1;

    public BankBranchesPresenterImpl(BankBranchesView bankBranchesView) {
        this.bankBranchesView = bankBranchesView;
    }

    @Override
    public void onCreate(Context context, Activity activity) {
        this.bankBranchesView.inflateToolbar();
        this.context = context;
        this.activity = activity;
        this.locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }

    @Override
    public void requestMyLocation() {
        this.requestAccessFineLocation();
    }

    @Override
    public void requestAccessFineLocation() {
        boolean withPermissions = Utils.checkSelfPermission(this.context, Manifest.permission.ACCESS_FINE_LOCATION);
        if (withPermissions) {
            setUpGClient();
            this.loadBankBranches();
        } else {
            Utils.requestPermission(this.context, this, GI.DENIED_MESSAGE, Manifest.permission.ACCESS_FINE_LOCATION);
        }
    }

    @Override
    public synchronized void setUpGClient() {
        if (this.googleApiClient == null || !this.googleApiClient.isConnected()) {
            this.googleApiClient = new GoogleApiClient.Builder(this.context)
                    .enableAutoManage((BranchesActivity) this.activity, 0, this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();

            googleApiClient.connect();
        }else
            setLocation();
    }

    @Override
    public void loadBankBranches() {
        branchesService.getBankBranches();
    }

    @Override
    public void onSuccessGetBranches(ArrayList<BankBranch> bankBranches) {

        for (int i = 0; i <= bankBranches.size() - 1; i++) {
            Location l = new Location(new String());
            l.setLatitude(Double.parseDouble(bankBranches.get(i).getLatitud()));
            l.setLongitude(Double.parseDouble(bankBranches.get(i).getLongitud()));
            if (myLocation != null)
                bankBranches.get(i).setDistance(Double.valueOf((this.myLocation.distanceTo(l) / 1000)));
        }

        Collections.sort(bankBranches, new Comparator<BankBranch>() {
            @Override
            public int compare(BankBranch bankBranch, BankBranch t1) {
                if (bankBranch.getDistance() != null && !bankBranch.getDistance().isNaN() && t1.getDistance() != null && !t1.getDistance().isNaN())
                    return Double.compare(bankBranch.getDistance(), t1.getDistance());
                return 0;
            }
        });
        this.bankBranchesView.paintMarkersBankBranches(bankBranches);
        this.bankBranchesView.createBankBranchesRecycler(bankBranches);
    }

    @Override
    public void onFailureGetBranches() {
    }

    @Override
    public void onErrorGetBranches() {
    }

    @Override
    public void onPermissionGranted() {
        this.requestMyLocation();
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermissions) {
        this.loadBankBranches();
    }

    private void setLocation() {

        Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        Location locationNet = this.locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        if (locationGPS != null)
            myLocation = locationGPS;
        else if (locationNet != null)
            myLocation = locationNet;

        else if (googleApiClient != null && googleApiClient.isConnected()){
            LocationRequest locationRequest = new LocationRequest();
            locationRequest.setInterval(3000);
            locationRequest.setFastestInterval(3000);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
            builder.setAlwaysShow(true);
            LocationServices.FusedLocationApi.requestLocationUpdates(this.googleApiClient, locationRequest, this);

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());

            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            int permissionLocation = ContextCompat.checkSelfPermission(context,Manifest.permission.ACCESS_FINE_LOCATION);
                            if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                                myLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                                if (myLocation != null)
                                    bankBranchesView.moveCameraToUserLocation(myLocation);
                            }
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                status.startResolutionForResult(activity, REQUEST_CHECK_SETTINGS_GPS);
                            } catch (IntentSender.SendIntentException e) {}
                            break;
                    }
                }
            });
        }

        if (this.myLocation != null)
            this.bankBranchesView.moveCameraToUserLocation(this.myLocation);

    }

   @Override
    public void onLocationChanged(Location location) {}

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        this.setLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {}

}
