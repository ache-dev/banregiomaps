package com.banregio.maps.ui.login;

import com.banregio.maps.beans.User;

/**
 * Created by AcheDev on 5/12/18.
 */
public class LoginPresenterImpl implements LoginPresenter, LoginService.LoginServiceListener {
    private LoginView loginView;
    private final LoginServiceImpl loginServiceImpl =  new LoginServiceImpl(this);

    public LoginPresenterImpl(LoginView loginView) {
        this.loginView = loginView;
    }

    @Override
    public void signIn(final User user) {
        this.loginView.setIsAuthenticating(true);
        if(user.isValidUser())
            this.loginServiceImpl.doLoginRequest(user);
        else
            this.loginView.setIsAuthenticating(false);
    }

    @Override
    public void onLoginSuccess() {
        if (loginView != null) {
            loginView.navigateToHome();
            this.loginView.setIsAuthenticating(false);
        }
    }

    @Override
    public void onLoginFailure() {
        if (loginView != null) {
            loginView.showToast("User or Password invalid");
            this.loginView.setIsAuthenticating(false);
        }
    }

    @Override
    public void onLoginError() {
        if (loginView != null) {
            loginView.showToast("Internal server error");
            this.loginView.setIsAuthenticating(false);
        }
    }
}
