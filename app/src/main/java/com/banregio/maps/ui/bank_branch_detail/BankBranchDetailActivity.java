package com.banregio.maps.ui.bank_branch_detail;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.banregio.maps.R;
import com.banregio.maps.beans.BankBranch;
import com.banregio.maps.databinding.ActivityBankBranchDetailBinding;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BankBranchDetailActivity extends AppCompatActivity implements BankBranchDetailView {

    @BindView(R.id.toolbar) Toolbar toolbar;

    public static final String BANK_BRANCH_FLAG = "BankBranch";
    private BankBranch bankBranch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityBankBranchDetailBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_bank_branch_detail);
        ButterKnife.bind(this);

        this.bankBranch = (BankBranch) getIntent().getExtras().getSerializable(BANK_BRANCH_FLAG);
        this.bankBranch.setContext(this);
        binding.setBankBranch(this.bankBranch);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

}
