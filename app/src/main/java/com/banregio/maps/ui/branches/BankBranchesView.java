package com.banregio.maps.ui.branches;

import android.location.Location;

import com.banregio.maps.beans.BankBranch;

import java.util.ArrayList;

/**
 * Created by AcheDev on 5/12/18.
 */

public interface BankBranchesView {

    void paintMarkersBankBranches(ArrayList<BankBranch> bankBranches);

    void createBankBranchesRecycler(ArrayList<BankBranch> bankBranches);

    void goToBankBranchDetail(BankBranch bankBranche);

    void inflateToolbar();

    void moveCameraToUserLocation(Location location);
}
