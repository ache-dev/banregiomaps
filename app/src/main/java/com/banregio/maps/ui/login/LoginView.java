package com.banregio.maps.ui.login;

import android.text.TextWatcher;
import android.view.View;

/**
 * Created by AcheDev on 5/12/18.
 */

public interface LoginView {

    View.OnClickListener signIn();

    TextWatcher inputUser();

    TextWatcher inputPassword();

    void showToast(String message);

    void navigateToHome();

    void setIsAuthenticating(boolean isAuthenticating);

    boolean isAuthenticating();

}
