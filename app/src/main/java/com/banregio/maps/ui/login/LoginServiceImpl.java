package com.banregio.maps.ui.login;

import com.banregio.maps.beans.User;
import com.banregio.maps.net.api.ManagerAPI;
import com.banregio.maps.net.callbacks.BanregioManagerCallback;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by AcheDev on 5/12/18.
 */

public class LoginServiceImpl implements LoginService, BanregioManagerCallback.LoginCallback {
    private LoginService.LoginServiceListener loginServiceListener;
    private User userIn;

    public LoginServiceImpl(LoginService.LoginServiceListener loginServiceListener){
        this.loginServiceListener = loginServiceListener;
    }
    @Override
    public void doLoginRequest(final User user) {
        this.userIn = user;
        ManagerAPI.getInstance().doLoginRequest(this);
    }

    @Override
    public void onResponse(Call<User> call, Response<User> response) {
        if (response.isSuccessful() && this.userIn.getUser().equals(response.body().getUser()) && this.userIn.getPassword().equals(response.body().getPassword()))
            loginServiceListener.onLoginSuccess();
        else
            loginServiceListener.onLoginFailure();
    }

    @Override
    public void onFailure(Call<User> call, Throwable t) {
        loginServiceListener.onLoginError();
    }
}
