package com.banregio.maps.ui.branches;

import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

import com.banregio.maps.beans.BankBranch;
import com.banregio.maps.net.api.ManagerAPI;
import com.banregio.maps.net.callbacks.BanregioManagerCallback;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by AcheDev on 5/13/18.
 */

public class BankBranchesServiceImpl implements BankBranchesService, BanregioManagerCallback.BankBranchesCallback {

    private String LOGCAT = this.getClass().getCanonicalName();

    private BankBranchesServiceListener bankBranchesServiceListener;

    public BankBranchesServiceImpl(BankBranchesServiceListener bankBranchesServiceListener) {
        this.bankBranchesServiceListener = bankBranchesServiceListener;
    }

    @Override
    public void getBankBranches() {
        ManagerAPI.getInstance().getBranchesBank(this);
    }

    @Override
    public void onResponse(Call<ArrayList<BankBranch>> call, Response<ArrayList<BankBranch>> response) {
        if (response.isSuccessful()) {
            bankBranchesServiceListener.onSuccessGetBranches(response.body());
        } else
            bankBranchesServiceListener.onFailureGetBranches();
    }

    @Override
    public void onFailure(Call<ArrayList<BankBranch>> call, Throwable t) {
        Log.i(LOGCAT, t.getMessage());
        bankBranchesServiceListener.onErrorGetBranches();
    }
}
