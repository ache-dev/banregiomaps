package com.banregio.maps.ui.branches;

import android.Manifest;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;

import com.banregio.maps.R;
import com.banregio.maps.beans.BankBranch;
import com.banregio.maps.ui.bank_branch_detail.BankBranchDetailActivity;
import com.banregio.maps.ui.branches.adapters.recyclers.BankBranchesAdapter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.banregio.maps.ui.branches.BankBranchesPresenterImpl.REQUEST_CHECK_SETTINGS_GPS;

public class BranchesActivity extends AppCompatActivity implements OnMapReadyCallback, BankBranchesView, SearchView.OnQueryTextListener, GoogleMap.OnMarkerClickListener {

    private final String LOG_CAT = this.getClass().getCanonicalName();
    @BindView(R.id.rcv_bank_branches)
    RecyclerView recyclerView;
    @BindView(R.id.n_toolbar)
    Toolbar toolbar;

    private GoogleMap mMap;
    private SearchView searchView;
    private BankBranchesAdapter bankBranchesAdapter;
    private final BankBranchesPresenterImpl bankBranchesPresenter = new BankBranchesPresenterImpl(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_branches);
        ButterKnife.bind(this);
        bankBranchesPresenter.onCreate(getApplicationContext(), this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void inflateToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        this.bankBranchesPresenter.requestMyLocation();
    }

    @Override
    public void moveCameraToUserLocation(Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11.0f));
        mMap.setMyLocationEnabled(true);
    }

    @Override
    public void paintMarkersBankBranches(ArrayList<BankBranch> bankBranches) {
        for (BankBranch bb : bankBranches) {
            LatLng llbb = new LatLng(Double.valueOf(bb.getLatitud()), Double.valueOf(bb.getLongitud()));

            Marker marker = mMap.addMarker(new MarkerOptions()
                        .position(llbb)
                        .title(bb.getNombre()));

            marker.setTag(bb);
            marker.setIcon(BitmapDescriptorFactory.fromResource(bb.getIdMarkerMipmap()));
        }
    }

    @Override
    public void createBankBranchesRecycler(ArrayList<BankBranch> bankBranches) {

        bankBranchesAdapter = new BankBranchesAdapter(bankBranches, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(bankBranchesAdapter);
    }

    @Override
    public void goToBankBranchDetail(BankBranch bankBranche) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(BankBranchDetailActivity.BANK_BRANCH_FLAG, bankBranche);
        Intent i = new Intent(getApplicationContext(), BankBranchDetailActivity.class);
        i.putExtras(bundle);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.branches_toolbar, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(this);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        bankBranchesAdapter.getFilter().filter(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        bankBranchesAdapter.getFilter().filter(newText);
        return false;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        bankBranchesAdapter.getFilter().filter(((BankBranch)marker.getTag()).getNombre());
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS_GPS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        this.bankBranchesPresenter.requestMyLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        break;
                }
                break;
        }
    }
}
