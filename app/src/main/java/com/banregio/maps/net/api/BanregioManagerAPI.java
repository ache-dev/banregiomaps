package com.banregio.maps.net.api;

import com.banregio.maps.beans.BankBranch;
import com.banregio.maps.beans.User;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by AcheDev on 4/23/18.
 */

public interface BanregioManagerAPI {

    @GET("login")
    Call<User> getLoginResource();

    @GET("sucursales")
    Call<ArrayList<BankBranch>> getBankBranches();

}
