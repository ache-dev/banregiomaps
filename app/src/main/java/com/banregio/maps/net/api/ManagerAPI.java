package com.banregio.maps.net.api;

import com.banregio.maps.beans.BankBranch;
import com.banregio.maps.beans.User;
import com.banregio.maps.net.BanregioNet;

import java.util.ArrayList;

import retrofit2.Callback;

/**
 * Created by AcheDev on 4/23/18.
 */
public final class ManagerAPI {

    private static ManagerAPI managerAPI;
    private BanregioManagerAPI banregioManagerAPI;

    private ManagerAPI (){
        banregioManagerAPI = BanregioNet.getClient().create(BanregioManagerAPI.class);
    }

    public static ManagerAPI getInstance(){
        return managerAPI = (managerAPI == null ? new ManagerAPI(): managerAPI);
    }

    public void doLoginRequest(Callback <User> callback){
        banregioManagerAPI
                .getLoginResource()
                .enqueue(callback);
    }

    public void getBranchesBank(Callback <ArrayList<BankBranch>> callback){
        banregioManagerAPI
                .getBankBranches()
                .enqueue(callback);
    }
}
