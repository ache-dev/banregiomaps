package com.banregio.maps.net.callbacks;

import com.banregio.maps.beans.BankBranch;
import com.banregio.maps.beans.User;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AcheDev on 5/7/18.
 */

public interface BanregioManagerCallback {

    interface LoginCallback extends Callback<User>{
        @Override
        void onResponse(Call<User> call, Response<User> response);

        @Override
        void onFailure(Call<User> call, Throwable t);
    }

    interface BankBranchesCallback extends Callback<ArrayList<BankBranch>>{
        @Override
        void onResponse(Call<ArrayList<BankBranch>> call, Response<ArrayList<BankBranch>> response);

        @Override
        void onFailure(Call<ArrayList<BankBranch>> call, Throwable t);
    }
}
